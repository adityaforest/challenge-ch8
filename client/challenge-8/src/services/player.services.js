import axios from 'axios';

const API_URL = 'http://localhost:5000/api/players';

class PlayerService {
    getAllPlayer(){
        return axios.get(API_URL)
    }

    getPlayerByUsername(username) {
        return axios.get(API_URL + `?username=${username}`);
    }

    getPlayerByEmail(email) {
        return axios.get(API_URL + `?email=${email}`);
    }

    getPlayerByExperience(exp) {
        return axios.get(API_URL + `?experience=${exp}`);
    }

    getPlayerByLevel(lvl) {
        return axios.get(API_URL + `?lvl=${lvl}`);
    }

    getPlayerById(id) {
        return axios.get(API_URL + `/${id}`);
    }

    createPlayer(username, email, password) {
        return axios.post(API_URL, { username: username, email: email, password: password })
    }

    editPlayer(id, username, email, password, exp, lvl) {
        return axios.put(API_URL + `/${id}`, { username: username, email: email, password: password, experience: exp, lvl: lvl })
    }

    deletePlayer(id) {
        return axios.delete(API_URL + `/${id}`)
    }

    addPlayerExperience(id, exp) {
        return axios.post(API_URL + `/exp/${id}`, { exp: exp })
    }
}

export default new PlayerService();
import React, { useEffect, useState } from 'react'
import { Link, useParams } from 'react-router-dom'
import playerServices from '../services/player.services'

function ViewPage() {
    const [username, setUsername] = useState('')
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [exp, setExp] = useState(0)
    const [lvl, setLvl] = useState(0)

    useEffect(() => {
        getPlayerData()
    }, [])

    const param = useParams()
    const getPlayerData = () => {
        playerServices.getPlayerById(param.id)
            .then(res => {
                setUsername(res.data.message.username)
                setEmail(res.data.message.email)
                setPassword(res.data.message.password)
                setExp(res.data.message.experience)
                setLvl(res.data.message.lvl)
            })
    }

    return (
        <>
            <div className="row border-bottom border-bottom-dark mb-3">
                <div className="col-md-4">
                    <Link className='btn btn-primary' to='/'> &lt; Back</Link>
                </div>
                <div className="col-md-8">
                    <h1 className='text-center'>View Player {param.id}</h1>
                </div>
            </div>
            <div className="d-flex justify-content-center">
                <img src="https://e7.pngegg.com/pngimages/176/778/png-clipart-onigiri-sushi-rice-%E4%B8%80%E3%82%B3%E3%83%9E%E6%BC%AB%E7%94%BB-kawai-sushi-smiley-sticker.png"
                    width='150px' className='mb-3' />
            </div>
            <div>
                <h3>Username : {username}</h3>
                <h3>Email : {email}</h3>
                <h3>Password : {password}</h3>
                <h3>Level : {lvl}</h3>
                <h3>Experience : {exp}</h3>
            </div>
        </>
    )
}

export default ViewPage
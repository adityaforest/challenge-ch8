import { BrowserRouter as Router, Routes, Route } from 'react-router-dom'
import CreatePage from './pages/CreatePage'
import EditPage from './pages/EditPage'
import MainPage from './pages/MainPage'
import ViewPage from './pages/ViewPage'

const style = {
  container: {
    width: "100vw",
    height: "100vh",
    backgroundColor: "black"
  },
  box:{
    width: "500px",
    height: "500px",
    backgroundColor: "white",
    borderRadius: "20px"   ,
    boxShadow: "0px 0px 100px 0px white",
    padding: "10px"
  }
}

function App() {
  return (
    <div className="app d-flex align-items-center justify-content-center" style={style.container}>
      <div className="box" style={style.box}>
        <Router>
          <Routes>
            <Route path='/' element={<MainPage />} />
            <Route path='/create' element={<CreatePage />} />
            <Route path='/edit/:id' element={<EditPage />} />
            <Route path='/view/:id' element={<ViewPage />} />
          </Routes>
        </Router>
      </div>
    </div>
  );
}

export default App;
